import { Component, HostListener } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  array = [[["pók"],["szellem"],["szemgolyó"],["kincs"]],[["játékos"],[], ["kígyó"],["kincs"]]];
  allowedKeys = ["w", "a", "s", "d", "h", "g"];
  playerRow = 1;
  playerColumn = 0;
  treasureGrabbed = false;
  counter = 0;
  error = "";
  key = "";
  snakeRow = 1;
  snakeColumn = 2;
  ghostRow = 0;
  ghostColumn = 1;
  spiderRow = 0;
  spiderColumn = 0;
  eyeRow = 0;
  eyeColumn = 2;
  snakeDirection = "left";
  steps = [];
  gameOver = false;
  tryOrder = ["a", "w", "d", "s", "h", "g"]
  winnerGames = [];
  validStep = false;
  saves = [];
  globalCounter = 0;

  @HostListener('document:keypress', ['$event'])
  handleKeyboardEvent(event: KeyboardEvent) {
    this.key = event.key;
    this.steps.push(this.key);
    this.validStep = false;
    this.step();
    if(this.validStep && this.key !== "g") {
      this.advanceGame();
    }
  }

  constructor() {
    // Remove comment if you would like to run AI, with up to 30 iterations it finds a solution to the problem, altough it takes a lot of time
    //this.ai(false);
  }

  public ai(isG: boolean) {
    this.saveGame(this.steps.length);
    if(this.steps.length > 30) {
      return false;
    }
    this.tryOrder.forEach(element => {
      this.globalCounter++;
      this.loadGame(this.steps.length);
      this.steps.push(element);

      if(isG && element === "g") {
        this.steps.splice(this.steps.length - 1, 1);
        return;
      }
      this.key = element;
      this.validStep = false;
      this.step();
      if(this.validStep) {
        if(this.array[0][0].includes("kincs") && this.array[1][0].includes("kincs")) {
            this.winnerGames.push(JSON.stringify(this.steps));
            return;
        }
        if(element !== "g") {
          this.advanceGame();
        }
        if(!this.gameOver) {
          this.ai(element === "g");
          this.steps.splice(this.steps.length - 1, 1);
        } else {
          this.steps.splice(this.steps.length - 1, 1);
          this.gameOver = false;
        }
      } else {
        this.steps.splice(this.steps.length - 1, 1);
      }
    });
    return true;
  }

  public trackItem (index: number, item: string[]) {
    return item.join();
  }

  private saveGame(saveNumber: number) {
    let findIndex = this.saves.findIndex(save => save.saveNumber === saveNumber);
    if(findIndex !== -1) {
      this.saves.splice(findIndex, 1);
    }
    this.saves.push({
      array: JSON.stringify(this.array),
      saveNumber: saveNumber,
      playerRow: this.playerRow,
      playerColumn: this.playerColumn,
      treasureGrabbed: this.treasureGrabbed,
      counter: this.counter,
      error: this.error,
      key: this.key,
      snakeRow: this.snakeRow,
      snakeColumn: this.snakeColumn,
      ghostRow: this.ghostRow,
      ghostColumn: this.ghostColumn,
      spiderRow: this.spiderRow,
      spiderColumn: this.spiderColumn,
      eyeRow: this.eyeRow,
      eyeColumn: this.eyeColumn,
      snakeDirection: this.snakeDirection
    })
  }

  private loadGame(saveNumber: number) {
    let saveGame = this.saves.find(save => save.saveNumber === saveNumber);
    if(saveGame) {
      this.array = JSON.parse(saveGame.array),
      this.playerRow = saveGame.playerRow,
      this.playerColumn = saveGame.playerColumn,
      this.treasureGrabbed = saveGame.treasureGrabbed,
      this.counter = saveGame.counter,
      this.error = saveGame.error,
      this.key = saveGame.key,
      this.snakeRow = saveGame.snakeRow,
      this.snakeColumn = saveGame.snakeColumn,
      this.ghostRow = saveGame.ghostRow,
      this.ghostColumn = saveGame.ghostColumn,
      this.spiderRow = saveGame.spiderRow,
      this.spiderColumn = saveGame.spiderColumn,
      this.eyeRow = saveGame.eyeRow,
      this.eyeColumn = saveGame.eyeColumn,
      this.snakeDirection = saveGame.snakeDirection
    }
  }

  private step(): boolean {
    if(this.allowedKeys.includes(this.key) && !this.gameOver) {
      if(this.key === "w") {
        if(this.playerRow == 0) {
          this.error = "Can't go up in top row";
          return;
        }
        let targetCell = this.array[this.playerRow - 1][this.playerColumn];
        if(targetCell.includes("pók") || targetCell.includes("kígyó") || targetCell.includes("szemgolyó") || targetCell.includes("szellem")) {
          this.error = "Top cell is already occupied";
          return;
        }
        if(targetCell.includes("kincs") && this.treasureGrabbed) {
          this.error = "You can't move the treasure to another treasure";
          return;
        }
        this.removeGamer();
        this.playerRow = this.playerRow - 1;
        this.validStep = true;
      }
      if(this.key === "s") {
        if(this.playerRow == 1) {
          this.error = "Can't go down in bottom row";
          return;
        }
        let targetCell = this.array[this.playerRow + 1][this.playerColumn];
        if(targetCell.includes("pók") || targetCell.includes("kígyó") || targetCell.includes("szemgolyó") || targetCell.includes("szellem")) {
          this.error = "Bottom cell is already occupied";
          return;
        }
        if(targetCell.includes("kincs") && this.treasureGrabbed) {
          this.error = "You can't move the treasure to another treasure";
          return;
        }
        this.removeGamer();
        this.playerRow = this.playerRow + 1;
        this.validStep = true;
      }
      if(this.key === "d") {
        if(this.playerColumn == 3) {
          this.error = "Can't go right in rightmost column";
          return;
        }
        let targetCell = this.array[this.playerRow][this.playerColumn + 1];
        if(targetCell.includes("pók") || targetCell.includes("kígyó") || targetCell.includes("szemgolyó") || targetCell.includes("szellem")) {
          this.error = "Column to right is already occupied";
          return;
        }
        if(targetCell.includes("kincs") && this.treasureGrabbed) {
          this.error = "You can't move the treasure to another treasure";
          return;
        }
        this.removeGamer();
        this.playerColumn = this.playerColumn + 1;
        this.validStep = true;
      }
      if(this.key === "a") {
        if(this.playerColumn == 0) {
          this.error = "Can't go left in leftmost column";
          return;
        }
        let targetCell = this.array[this.playerRow][this.playerColumn - 1];
        if(targetCell.includes("pók") || targetCell.includes("kígyó") || targetCell.includes("szemgolyó") || targetCell.includes("szellem")) {
          this.error = "Column to left is already occupied";
          return;
        }
        if(targetCell.includes("kincs") && this.treasureGrabbed) {
          this.error = "You can't move the treasure to another treasure";
          return;
        }
        this.removeGamer();
        this.playerColumn = this.playerColumn - 1;
        this.validStep = true;
      }
      if(this.key === "g") {
        if(!this.array[this.playerRow][this.playerColumn].includes("kincs")) {
          this.error = "There is no treasure at this location";
          return;
        }
        this.treasureGrabbed = !this.treasureGrabbed;
        this.validStep = true;
        return;
      }
      if(this.key !== "h") {
        this.array[this.playerRow][this.playerColumn].push("játékos");
        if(this.treasureGrabbed) {
          this.array[this.playerRow][this.playerColumn].push("kincs");
        }
      }
      this.validStep = true;
    }
  }

  private removeGamer() {
    this.array[this.playerRow][this.playerColumn].splice(this.array[this.playerRow][this.playerColumn].findIndex(element => element === "játékos"), 1);
    if(this.treasureGrabbed) {
      this.array[this.playerRow][this.playerColumn].splice(this.array[this.playerRow][this.playerColumn].findIndex(element => element === "kincs"), 1);
    }
  }

  private removeString(row: number, column: number, name: string) {
    this.array[row][column].splice(this.array[row][column].findIndex(element => element === name), 1);
  }

  private advanceGame() {
    this.error = "";
    this.counter++;
    this.advanceGhost();
    this.advanceEye();
    this.advanceSpider();
    this.advanceSnake();
    this.array[this.snakeRow][this.snakeColumn].push("kígyó");
  }

  private advanceGhost() {
    let isEmpty = false;
    let j = this.ghostColumn;
    let i = this.ghostRow;
    while(!isEmpty) {
      if(this.array[i][j].length === 0) {
        isEmpty = true;
        continue;
      }
      if(i === 0) {
        if(j === 3) {
          i++;
        } else {
          j++;
        }
      } else {
        if(j == 0) {
          i--;
        } else {
          j--;
        }
      }
    }
    this.removeString(this.ghostRow, this.ghostColumn, "szellem");
    this.ghostRow = i;
    this.ghostColumn = j;
    this.array[this.ghostRow][this.ghostColumn].push("szellem");
  }

  private advanceSpider() {
    this.removeString(this.spiderRow, this.spiderColumn, "pók");
    if(this.spiderRow === 0) {
      if(this.spiderColumn === 0) {
        this.spiderColumn = 3;
      } else {
        this.spiderRow = 1;
      }
    } else {
      if(this.spiderColumn === 3) {
        this.spiderColumn = 0;
      } else {
        this.spiderRow = 0;
      }
    }
    this.array[this.spiderRow][this.spiderColumn].push("pók");
    if(this.array[this.spiderRow][this.spiderColumn].includes("játékos")) {
      this.gameOver = true;
      return;
    }
  }

  private advanceEye() {
    let counter = 1;
    this.removeString(this.eyeRow, this.eyeColumn, "szemgolyó");
    if(this.snakeDirection === "left") {
      while((this.eyeColumn - counter) > 0 && this.array[this.eyeRow][this.eyeColumn - counter].length === 0) {
        counter++;
      }
      this.eyeColumn = Math.max(this.eyeColumn - counter, 0);
    }
    if(this.snakeDirection === "right") {
      while((this.eyeColumn + counter) < 3 && this.array[this.eyeRow][this.eyeColumn + counter].length === 0) {
        counter++;
      }
      this.eyeColumn = Math.min(this.eyeColumn + counter, 3);
    }
    if(this.snakeDirection === "up") {
      while((this.eyeRow - counter) > 0 && this.array[this.eyeRow - counter][this.eyeColumn].length === 0) {
        counter++;
      }
      this.eyeRow = Math.max(this.eyeRow - counter, 0);
    }
    if(this.snakeDirection === "down") {
      while((this.eyeRow + counter) < 1 && (this.array[this.eyeRow + counter][this.eyeColumn].length === 0)) {
        counter++;
      }
      this.eyeRow = Math.min(this.eyeRow + counter, 1);
    }
    this.array[this.eyeRow][this.eyeColumn].push("szemgolyó");
    if(this.array[this.eyeRow][this.eyeColumn].includes("játékos")) {
      this.gameOver = true;
      return;
    }
  }

  private advanceSnake() {
    this.removeString(this.snakeRow, this.snakeColumn, "kígyó");
    if(this.snakeDirection === "left") {
      if(this.snakeColumn - 1 >= 0 && this.array[this.snakeRow][this.snakeColumn - 1].length === 0) {
        this.snakeColumn = this.snakeColumn - 1;
      } else {
        this.snakeDirection = "up";
      }
      return;
    }
    if(this.snakeDirection === "right") {
      if(this.snakeColumn + 1 <= 3 && this.array[this.snakeRow][this.snakeColumn + 1].length == 0) {
        this.snakeColumn = this.snakeColumn + 1;
      } else {
        this.snakeDirection = "down";
      }
      return;
    }
    if(this.snakeDirection === "up") {
      if(this.snakeRow - 1 >= 0 && this.array[this.snakeRow - 1][this.snakeColumn].length == 0) {
        this.snakeRow = this.snakeRow - 1;
      } else {
        this.snakeDirection = "right";
      }
      return;
    }
    if(this.snakeDirection === "down") {
      if(this.snakeRow + 1 <= 1 && this.array[this.snakeRow + 1][this.snakeColumn].length == 0) {
        this.snakeRow = this.snakeRow + 1;
      } else {
        this.snakeDirection = "left";
      }
      return;
    }
  }

}
