# Game1

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.1.3.

## Requirements

Node and npm ( https://nodejs.org/en/download/ )

## Instructions

Just run `npm i` and start the Development server

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Description

This was made for the 90th "ész ventura" puzzle on Qubit ( https://qubit.hu/2020/05/31/esz-ventura-hello-kalandor-juttasd-el-a-kincset-a-megfelelo-szobakba )
The application includes the implementation of the game playable by the user. You can also run an AI by uncommenting `this.ai(false)` method in the constructor of AppComponent `app.component.ts`. It will run a path finding algorithm with max 30 steps iteration. It takes a long time to run, the results can be read in the `winnerGames` array. Fastest result is `["d","h","d","d","g","a","h","w","h","g","s","w","d","g","s","a","h","g","w","g","a","s","a","g","d","d","g","a","w","a"]"` which is 30 steps or 23 steps if we do not count the treasure grabbing/dropping. 

## Improvement opportunities
Note that this program was made to quickly solve the problem at hand, not to find the quickest algorithm. It can be improved a lot. Also the AI should be implemented in other language, js is not really the best tool for it.
